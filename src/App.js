import "./App.css";
import Header from "./components/Header";
import Todo from "./components/Todo";
function App() {
  return (
    <div className="App" data-testid="app">
      <Header />
      <Todo />
    </div>
  );
}

export default App;
