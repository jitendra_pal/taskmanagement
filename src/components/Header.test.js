import { render, screen } from "@testing-library/react";
import Header from "./Header";

const doRender = () => {
  render(<Header />);
};
describe("render header component", () => {
  test("check logo text exists", () => {
    doRender();
    const logotext = screen.getByText("Jira board");
    expect(logotext).toBeInTheDocument();
  });
  test("check task management text exists", () => {
    doRender();
    const taskText = screen.getByText("Task Management App");
    expect(taskText).toBeInTheDocument();
  });
});
