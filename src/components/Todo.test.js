import { render, screen } from "@testing-library/react";
import Todo from "./Todo";

const doRender = () => {
  render(<Todo />);
};
describe("render TODO component", () => {
  test("check Add Task text exists", () => {
    doRender();
    const addtext = screen.getByText("Add Task");
    expect(addtext).toBeInTheDocument();
  });
  test("check Enter Title text exists", () => {
    doRender();
    const title = screen.getByText("Enter Title");
    expect(title).toBeInTheDocument();
  });
});
